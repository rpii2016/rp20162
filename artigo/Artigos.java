package artigo;

import java.util.ArrayList;
import java.util.List;
import rp2.ListaSubmissoes;
import rp2.Submissao;

/**
 * Classe que reune os principais metodos de funcionabilidade dos objetos salvos no sistema.
 * @author Bruno Oliveira
 *
 */
public class Artigos  implements ListaSubmissoes {

    private static List<Submissao> ListaArtigo = new ArrayList<>();
    
     /**
     * Inclui uma submissão a lista de submissões.
     * @param submissao Submissão a ser incluída.
     * @return True se a inclusão foi bem sucedida ou False em caso contrário. 
     */
    @Override
    public boolean incluir(Submissao submissao) {
        if (submissao==null) {
            return false;
        } else {
            ListaArtigo.add(submissao);
            return true;
        }
    }
    
     /**
     * Consulta a submissão com o título informado.
     * @param titulo Título a ser consultado.
     * @return Submissão com o título informado ou null em caso de não existir submissão com o título informado. 
     */
    @Override
    public Submissao consultarTitulo(String titulo) {
        Submissao sub = null;
        for (Submissao categoria: ListaArtigo) {
            if (categoria.getTitulo().equalsIgnoreCase(titulo)) {
                sub = categoria;
            }
        }
        return sub;
    }

    /**
     * Consulta a lista de submissões com o autor informado.
     * @param autor Autor a ser consultado.
     * @return Lista de submissões do autor informado ou null em caso de não existir submissões do autor. 
     */
    @Override
    public List<Submissao> consultarAutor(String autor) {
        List<Submissao> sub = new ArrayList();
        for (Submissao categoria: ListaArtigo) {
            for (int i=0; i<categoria.getAutor().size(); i++) {
                if (categoria.getAutor().get(i).equalsIgnoreCase(autor)) {
                    sub.add(categoria);
                }
            }
        }
        return sub;
    }
    
    /**
     * Edita a submissão com o título informado.
     * @param titulo Título da submissão a ser editada.
     * @param submissao Submissão com as novas informações.
     * @return True se a submissão com o título informado for editada e 
     * False caso nenhuma submissão com o título infornado for encontrada.
     */
    @Override
    public boolean editar(String titulo, Submissao submissao) {
        if (consultarTitulo(titulo)==null) {
            return false;
        } else {
            int indice = ListaArtigo.indexOf(consultarTitulo(titulo));
            ListaArtigo.set(indice, submissao);
            return true;
        }
    }
    
    /**
     * Exclui a submissão com o título informado.
     * @param titulo Título da submissão a ser excluída.
     * @return True se a submissão com o título informado for excluída e 
     * False caso nenhuma submissão com o título infornado for encontrada.
     */
    @Override
    public boolean excluir(String titulo) {
        Artigos inter = new Artigos();
        Submissao sub = inter.consultarTitulo(titulo);
        if (sub==null) {
            return false;
        } else {
            ListaArtigo.remove(sub);
        }
        return true;
    }
    
}