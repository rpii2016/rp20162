package artigo;

import java.util.*;
import rp2.Situacao;
import rp2.SubmissaoCientifica;

/**
 * Classe para objetos do tipo Artigo, onde serão contidos valores e métodos para o mesmo.
 * @author Bruno Oliveira
 *
 */
public class Artigo extends SubmissaoCientifica {
    private String resumo;
    private String abstractt;
    
    public Artigo (String titulo, Situacao situacao, List<String> autores, int maxAutores, List<String> instituicao, 
    List<String> palavraChave, int maxInst, int maxPalavras, String resumo, String abstractt){
        
        super (titulo, situacao, autores, 8, instituicao, palavraChave, 8, 4);
        this.resumo = resumo;
        this.abstractt = abstractt;
    }
    
    /**
     * @return the resumo
     */
    public String getResumo() {
        return resumo;
    }
    /**
     * @param resumo the resumo to set
     */
    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    /**
     * @return the abstractt
     */
    public String getAbstractt() {
        return abstractt;
    }
    /**
     * @param abstractt the abstractt to set
     */
    public void setAbstractt(String abstractt) {
        this.abstractt = abstractt;
    }

                @Override
    public String toString(){
        String resp = super.toString() + "Resumo: "  + getResumo()  + "\nAbstract: "  + getAbstractt() +
        "\n##############################\n";
        return resp;
    }

}