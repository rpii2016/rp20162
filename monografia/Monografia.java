package monografia;

import java.util.ArrayList;
import java.util.List;
//import static rp2.Base.*;
//import rp2.Base.*;
import rp2.Situacao;
import rp2.Submissao;
import rp2.SubmissaoCientifica;

/**
 * Classe para manipulação de objetos Monografia.
 * @author Maurício El uri
 *
 */
public class Monografia extends SubmissaoCientifica {

    private int tipo;

    private String orientador;
    private String curso;
    private int ano;
    private int numPaginas;
    private String resumo;
    private String abstractt;

    /**
     * Construtor da Monografia
     *
     * @param tipo
     * @param orientador
     * @param curso
     * @param ano
     * @param numPaginas
     * @param resumo
     * @param abstractt
     * @param titulo
     * @param situacao
     * @param autores
     * @param maxAutores
     * @param instituicao
     * @param palavraChave
     * @param maxInst
     * @param maxPalavras
     */
    public Monografia(int tipo, String orientador, String curso, int ano, int numPaginas, String resumo, String abstractt, String titulo, Situacao situacao, List<String> autores, int maxAutores, List<String> instituicao, List<String> palavraChave, int maxInst, int maxPalavras) {
        super(titulo, situacao, autores, maxAutores, instituicao, palavraChave, maxInst, maxPalavras);
        this.tipo = tipo;
        this.orientador = orientador;
        this.curso = curso;
        this.ano = ano;
        this.numPaginas = numPaginas;
        this.resumo = resumo;
        this.abstractt = abstractt;
    }

    /**
     * @return the tipo
     */
    public int getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the orientador
     */
    public String getOrientador() {
        return orientador;
    }

    /**
     * @param orientador the orientador to set
     */
    public void setOrientador(String orientador) {
        this.orientador = orientador;
    }

    /**
     * @return the curso
     */
    public String getCurso() {
        return curso;
    }

    /**
     * @param curso the curso to set
     */
    public void setCurso(String curso) {
        this.curso = curso;
    }

    /**
     * @return the ano
     */
    public int getAno() {
        return ano;
    }

    /**
     * @param ano the ano to set
     */
    public void setAno(int ano) {
        this.ano = ano;
    }

    /**
     * @return the numPaginas
     */
    public int getNumPaginas() {
        return numPaginas;
    }

    /**
     * @param numPaginas the numPaginas to set
     */
    public void setNumPaginas(int numPaginas) {
        this.numPaginas = numPaginas;
    }

    /**
     * @return the resumo
     */
    public String getResumo() {
        return resumo;
    }

    /**
     * @param resumo the resumo to set
     */
    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    /**
     * @return the abstractt
     */
    public String getAbstractt() {
        return abstractt;
    }

    /**
     * @param abstractt the abstractt to set
     */
    public void setAbstractt(String abstractt) {
        this.abstractt = abstractt;
    }

    /**
     * @return the ListaMonografias
     */
    public static List<Monografia> getListaMonografias() {
        return ListaMonografias;
    }

    /**
     * @param aListaMonografias the ListaMonografias to set
     */
    public static void setListaMonografias(List<Monografia> aListaMonografias) {
        ListaMonografias = aListaMonografias;
    }

    /**
     * Criação da ListaMonografias
     */
    private static List<Monografia> ListaMonografias = new ArrayList<>();

    /**
     * Retorna a ListaMonografia
     *
     * @return ListaMonografia
     */
    public static List<Monografia> getListaMonografia() {
        return getListaMonografias();
    }

    /**
     * Insere uma Submissão monografia na ListaMonografia
     *
     * @param sub
     * @return boolean
     */
    public static boolean monoNaLista(Submissao sub) {
        return getListaMonografias().add((Monografia) sub);
    }

    /**
     * Recebe uma submissão e exclui Monografia da ListaMonografia.
     *
     * @param sub
     * @return boolean
     */
    public static boolean exclui(Submissao sub) {
        return getListaMonografias().remove(sub);
    }  
    
    /**
     * Função que retorna o tamanho da lista de palestraso
     *
     * @return tamanho
     */
    public static int sizeMono() {
        return getListaMonografias().size();

    }

    /**
     * Retorna um texto com os dados salvos em uma linha da lista
     *
     * @return Um texto formatado com todos os atributos do objeto do tipo
     * Monografia.
     */
    @Override
    public String toString() {
        String resp = super.toString()
                + "tipo: " + getTipo()
                + "\norientador: " + getOrientador()
                + "\ncurso: " + getCurso()
                + "\nano: " + getAno()
                + "\nNúmero de páginas: " + getNumPaginas()
                + "\nResumo: " + getResumo()
                + "\nAbstract: " + getAbstractt()
                + "\n##############################\n";
        return resp;
    }
}
