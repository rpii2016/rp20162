/**
 * Interface da Monografia
 *
 * @author Maurício El uri
 *
 */
package monografia;

import java.util.ArrayList;
import java.util.List;
import static monografia.Monografia.*;
import static rp2.Base.*;

import rp2.ListaSubmissoes;
import rp2.Situacao;
import rp2.Submissao;

/**
 * Classe que reúne os principais rp2 de funcionabilidade dos objetos salvos no
 * sistema.
 */
public class MonografiaInterface implements ListaSubmissoes {

    /**
     * Consulta a submissão com o título informado.
     *
     * @param titulo Título a ser consultado.
     * @return Submissão com o título informado ou null em caso de não existir
     * submissão com o título informado.
     */
    @Override
    public Submissao consultarTitulo(String titulo) {
        Submissao sub = null;
        for (Submissao categoria : getListaMonografias()) {
            if (categoria.getTitulo().toLowerCase().contains(titulo.toLowerCase())) {
                sub = categoria;
            }
        }
        return sub;
    }

    @Override
    public List<Submissao> consultarAutor(String autor) {
        List<Submissao> sub = new ArrayList();
        for (Submissao categoria : getListaMonografias()) {
            for (int i = 0; i < categoria.getAutor().size(); i++) {
                if (categoria.getAutor().get(i).toLowerCase().contains(autor.toLowerCase())) {
                    sub.add(categoria);
                }
            }
        }
        return sub;
    }

    /**
     * Função que recebe valores até ser digitado um número inteiro entre um e 2
     *
     * @return inteiro 1 ou 2
     */
    public static int escolhePesquisa() {
        int val;
        do {
            mensagem("\n[1] Pesquisar por título\n[2] Pesquisar por autor\n");
            val = lerInteiro("uma opção: ");
            if (val != 1 && val != 2) {
                mensagem("Valor inválido;");
            }
        } while (val != 1 && val != 2);
        return val;
    }

    /**
     * Compara um texto digitado pelo usuário com os objetos salvos no sistema.
     * Caso o texto digitado contenha em algum objeto do sistema, retorna o(s)
     * objeto(s). Caso o texto digitado não contenha nos objetos salvos envia
     * uma mensagem para o usuário. Caso ainda não tenham sido adicionados
     * objetos ao sistema, envia uma mensagem para o usuário.
     *
     * @return Se encontrou ou não o texto digitado em algum dos objetos.
     */
    public boolean pesquisar() {
        int tipo = 0;
        int opcao = escolhePesquisa();

        String pesquisa = lerString(" um texto para ser pesquisado: ");

        switch (opcao) {
            case 1:
                Submissao sub = consultarTitulo(pesquisa);
                if (sub == null) {
                    mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n");
                    return false;
                } else {
                    mensagem(sub.toString() + "\n");
                    tipo = 1;
                }
                break;
            case 2:
                List<Submissao> submissao = consultarAutor(pesquisa);
                if (submissao.isEmpty()) {
                    mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n");
                    return false;
                } else {
                    for (Submissao autores : submissao) {
                        mensagem(autores.toString() + "\n");
                        tipo = 2;
                    }
                }
                break;
        }
        return true;
    }

    /**
     * Exclui um objeto Monografia já salvo no sistema.
     *
     * @return
     */
    public boolean excluirInterface() {
        String pesquisa = lerString("o título para pesquisar:");
        Submissao sub = consultarTitulo(pesquisa);
        if (sub == null) {
            mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n");
            return false;
        } else {
            mensagem(sub.toString() + "\n");
        }
        String conf;
        do {
            conf = lerString("se quer excluir (s/n): ");
            if (!conf.equals("s") && !conf.equals("n")) {
                mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!\n");
            }
        } while (!conf.equals("s") && !conf.equals("n"));
        switch (conf) {
            case "s":
                if (excluir(pesquisa) == true) {
                    mensagem("Excluido com sucesso!");
                } else {
                    mensagem("Erro!");
                }
            case "n":
                mensagem("\n");
                break;
        }
        return true;
    }

    /**
     * Adiciona valores aos atributos de um objeto Monografia conforme informado
     * pelo usuário.
     */
    public void incluirMenu() {
        String titulo, orientador, curso, resumo, abstractt;
        int situacao, tipo, ano, numPaginas, palavras;
        ArrayList autor = new ArrayList();
        ArrayList instituicao = new ArrayList();
        ArrayList palavraChave = new ArrayList();
        Situacao situacaoO = null;
        mensagem("\n");
        titulo = lerString("o título: ");

        do {
            mensagem("[1] Sob Avaliação\n[2] Aprovado\n[3] Reprovado\n");
            situacao = lerInteiro("a situação: ");
            switch (situacao) {
                case 1:
                    situacaoO = Situacao.SOBAVALIACAO;
                    break;
                case 2:
                    situacaoO = Situacao.APROVADO;
                    break;
                case 3:
                    situacaoO = Situacao.REPROVADO;
                    break;
                default:
                    mensagem("\nVocê digitou um valor inválido.\n");
                    break;
            }
        } while (situacao <= 0 || situacao > 3);

        do {
            mensagem("[1] Graduação\n[2] Especialização\n[3] Mestrado\n[4] Doutorado\n");
            tipo = lerInteiro("o tipo: ");
            if (tipo <= 0 || tipo > 4) {
                mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!\n");
            }
        } while (tipo <= 0 || tipo > 4);

        autor.add(lerString("o autor: "));
        instituicao.add(lerString("a instituição referente a " + autor + ": "));
        orientador = lerString("o orientador: ");
        curso = lerString("o curso: ");
        ano = lerInteiro("o ano: ");
        numPaginas = lerInteiro("o número de páginas: ");

        do {
            palavras = lerInteiro("o número de palavras-chave que você deseja inserir: ");
            if (palavras <= 0 || palavras > 4) {
                mensagem("VOCE ADICIONAR ENTRE 1 A 4 PALAVRAS-CHAVE!\n\n");
            }
        } while (palavras <= 0 || palavras > 4);

        for (int z = 0; z < palavras; z++) {
            palavraChave.add(lerString("a " + (z + 1) + "º palavra-chave: "));
        }

        resumo = lerString("o resumo: ");
        abstractt = lerString("o abstract: ");
        Submissao submissao;
        submissao = new Monografia(tipo, orientador, curso, ano, numPaginas, resumo, abstractt, titulo, situacaoO, autor, 1, instituicao, palavraChave, 1, 1);

        incluir(submissao);
        mensagem("MONOGRAFIA ADICIONADA COM SUCESSO!\n");
    }

    /**
     * Edita e/ou adiciona os valores dos atributos de um determinado objeto
     * Monografia já salvo no sistema.
     *
     * @return true se for editar por um valor - else se nenhum resultado for
     * encontrado
     */
    @Override
    public boolean editar(String titulo, Submissao submissao) {
        if (submissao == null) {
            mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n\n");
            return false;
        } else {
            mensagem(submissao.toString());

            Monografia mono = (Monografia) submissao;

            int categoria, situacao;
            Situacao newsituacao;

            do {
                mensagem("\n[1] Título\n[2] Situação\n[3] Autor\n[4] Instituição\n[5] Palavra-Chave\n[6] Tipo\n[7] Orientador\n[8] Curso\n[9] Ano\n[10] Número de Páginas\n[11] Resumo\n[12] Abstract\n[0] Sair\n");
                categoria = lerInteiro("a categoria para editar: ");
                if (categoria > 12 || categoria < 0) {
                    mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!\n");
                }
            } while (categoria > 12 || categoria < 0);

            switch (categoria) {

                case 1:
                    mono.setTitulo(lerString("o novo título:"));
                    mensagem("\nCATEGORIA EDITADA COM SUCESSO!\n\n");
                    break;
                case 2:
                    situacao = LerSituacao();
                    switch (situacao) {
                        case 1:
                            newsituacao = Situacao.SOBAVALIACAO;
                            mono.setSituacao(newsituacao);
                            break;

                        case 2:
                            newsituacao = Situacao.APROVADO;
                            mono.setSituacao(newsituacao);
                            break;

                        case 3:
                            newsituacao = Situacao.REPROVADO;
                            mono.setSituacao(newsituacao);
                            break;
                    }
                    mensagem("\nCATEGORIA EDITADA COM SUCESSO!\n\n");
                    break;
                case 3:
                    List<String> newautor = mono.getAutor();
                    newautor.remove(0);
                    newautor.add(0, lerString("o novo autor: "));
                    mensagem("\nCATEGORIA EDITADA COM SUCESSO!\n\n");
                    break;
                case 4:
                    List<String> newInst = mono.getInstituicao();
                    newInst.remove(0);
                    newInst.add(0, lerString("a nova instituição: "));
                    mensagem("\nCATEGORIA EDITADA COM SUCESSO!\n\n");
                    break;
                case 5:
                    String showWord = "";
                    int respostaWord,
                     cont3 = 0;
                    List<String> newWord = mono.getPalavraChave();
                    for (int i = 0; i < mono.getPalavraChave().size(); i++) {
                        showWord += "[" + (i + 1) + "] " + mono.getPalavraChave().get(i) + "\n";
                        cont3++;
                    }

                    mensagem("\n" + showWord + "[0] Adicionar\n");

                    respostaWord = lerInteiro("a opção desejada: ");
                    if (respostaWord <= cont3) {
                        switch (respostaWord) {
                            case 0:
                                if (cont3 >= 4) {
                                    mensagem("Você já adicionou o número máximo de palavras chaves!");
                                } else {
                                    newWord.add((cont3), lerString("a nova palavra chave: "));
                                }
                                break;
                            case 1:
                                newWord.remove(0);
                                newWord.add(0, lerString("a nova palavra chave: "));
                                break;

                            case 2:
                                newWord.remove(1);
                                newWord.add(1, lerString("a nova palavra chave: "));
                                break;

                            case 3:
                                newWord.remove(2);
                                newWord.add(2, lerString("a nova palavra chave: "));
                                break;

                            case 4:
                                newWord.remove(3);
                                newWord.add(3, lerString("a nova palavra chave: "));
                                break;
                        }
                    }
                    mono.setPalavraChave(newWord);
                    mensagem("\nCATEGORIA EDITADA COM SUCESSO!\n\n");
                    break;
                case 6:
                    mono.setTipo(LerTipo());
                    mensagem("\nCATEGORIA EDITADA COM SUCESSO!\n\n");
                    break;
                case 7:
                    mono.setOrientador(lerString("o novo orientador: "));
                    mensagem("\nCATEGORIA EDITADA COM SUCESSO!\n\n");
                    break;
                case 8:
                    mono.setCurso(lerString("o novo curso: "));
                    mensagem("\nCATEGORIA EDITADA COM SUCESSO!\n\n");
                    break;
                case 9:
                    mono.setAno(lerInteiro("o novo ano: "));
                    mensagem("\nCATEGORIA EDITADA COM SUCESSO!\n\n");
                    break;
                case 10:
                    mono.setNumPaginas(lerInteiro("o novo número de páginas: "));
                    mensagem("\nCATEGORIA EDITADA COM SUCESSO!\n\n");
                    break;
                case 11:
                    mono.setResumo(lerString("o novo resumo: "));
                    mensagem("\nCATEGORIA EDITADA COM SUCESSO!\n\n");
                    break;
                case 12:
                    mono.setAbstractt(lerString("o novo abstract: "));
                    mensagem("\nCATEGORIA EDITADA COM SUCESSO!\n\n");
                    break;
            }
        }
        return true;
    }

    public static int LerSituacao() {
        int situacao;
        do {
            mensagem("\n[1] Sob Avaliação\n[2] Aprovado\n[3] Reprovado\n");
            situacao = lerInteiro("a nova situação: ");
            if (situacao <= 0 || situacao > 3) {
                mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!\n");
            }
        } while (situacao <= 0 || situacao > 3);
        return situacao;
    }

    public static int LerTipo() {
        int tipo;
        do {
            mensagem("\n[1] Graduação\n[2] Especialização\n[3] Mestrado\n[4] Doutorado\n");
            tipo = lerInteiro("o novo tipo: ");
            if (tipo <= 0 || tipo > 4) {
                mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!\n");
            }
        } while (tipo <= 0 || tipo > 4);
        return tipo;
    }

    /**
     * Inclui uma submissão a lista de submissões.
     *
     * @param submissao Submissão a ser incluída.
     * @return True se a inclusão foi bem sucedida ou False em caso contrário.
     */
    @Override
    public boolean incluir(Submissao submissao) {
        return monoNaLista(submissao);
    }

    /**
     * Exclui a submissão com o título informado.
     *
     * @param titulo Título da submissão a ser excluída.
     * @return True se a submissão com o título informado for excluída e False
     * caso nenhuma submissão com o título infornado for encontrada.
     */
    @Override
    public boolean excluir(String titulo) {
        Submissao sub = consultarTitulo(titulo);
        return exclui(sub);

    }

}
