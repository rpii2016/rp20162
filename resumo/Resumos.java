package resumo;

import java.util.ArrayList;
import java.util.List;
import static rp2.Base.*;
import rp2.ListaSubmissoes;
import rp2.Situacao;
import rp2.Submissao;

/**
 * Classe que reune os principais metodos de funcionabilidade dos objetos do
 * tipo Resumo salvos no sistema.
 *
 * @author Huillian Serpa
 *
 */
public class Resumos implements ListaSubmissoes {

    private static List<Submissao> ListaResumo = new ArrayList<>();
    
    /**
     * Através de parametros passados pelo usuario, um objeto do tipo Resumo eh
     * preenchido
     */
    @Override
        public boolean incluir(Submissao submissao) {
        if (submissao==null) {
            return false;
        } else {
            ListaResumo.add(submissao);
            return true;
        }
    }
    /*
    @Override
    public boolean incluir(Submissao submissao) {
        String titulo;
        int autores, palavras, situ;
        Situacao situacao = null;
        ArrayList autor = new ArrayList();
        ArrayList instituicao = new ArrayList();
        ArrayList palavrac = new ArrayList();

        mensagem("\n");
        titulo = lerString("o título: ");

        do {
            mensagem("[1] Sob Avaliação\n[2] Aprovado\n[3] Reprovado\n");
            situ = lerInteiro("a situação: ");
            switch (situ) {
                case 1:
                    situacao = Situacao.SOBAVALIACAO;
                    break;

                case 2:
                    situacao = Situacao.APROVADO;
                    break;

                case 3:
                    situacao = Situacao.REPROVADO;
                    break;

                default:
                    mensagem("\nVOCÊ DIGITOU UM VALOR INVÁLIDO!\n");
                    break;
            }
        } while (situ <= 0 || situ > 3);

        do {
            autores = lerInteiro("o número de autores que voce deseja inserir:");
            if (autores <= 0 || autores > 8) {
                mensagem("VOCE DEVE ADICIONAR ENTRE 1 A 8 AUTORES!\n\n");
            }
        } while (autores <= 0 || autores > 8);

        for (int i = 0; i < autores; i++) {
            autor.add(lerString("o " + (i + 1) + "º autor: "));
        }

        for (int x = 0; x < autores; x++) {
            instituicao.add(lerString("a instituição referente ao " + autor.get(x) + ": "));
        }

        do {
            palavras = lerInteiro("o número de palavras-chave que você deseja inserir: ");
            if (palavras <= 0 || palavras > 4) {
                mensagem("\nVOCE ADICIONAR ENTRE 1 A 4 PALAVRAS-CHAVE!\n");
            }
        } while (palavras <= 0 || palavras > 4);

        for (int z = 0; z < palavras; z++) {
            palavrac.add(lerString("a " + (z + 1) + "º palavra-chave: "));
        }

        submissao = new Resumo(titulo, situacao, autor, autores, instituicao, palavrac, autores, palavras);
        ListaResumo.add(submissao);
        mensagem("\nRESUMO ADICIONADO COM SUCESSO!\n");
        return true;
    }
*/
    /**
     * Atraves da categoria escolhida pelo usuário, a mesma eh editada
     *
     * @param sub
     * @return
     */
    @Override
     public boolean editar(String titulo, Submissao submissao) {
        if (consultarTitulo(titulo)==null) {
            return false;
        } else {
            int indice = ListaResumo.indexOf(consultarTitulo(titulo));
            ListaResumo.set(indice, submissao);
            return true;
        }
    }
    /*
    public boolean editar(String titulo, Submissao sub) {

        if (sub == null) {
            mensagem("NENHUM RESULTADO FOI ENCONTRADO!");
            return false;
        } else {
            mensagem(sub.toString());
            Resumo res = (Resumo) sub;
            String newtitulo;
            Situacao newsituacao;
            int categoria, newsitu;
            do {
                mensagem("\n[1] Título\n[2] Situação\n[3] Autor\n[4] Instituição\n[5] Palavra-Chave\n[0] Sair\n");
                categoria = lerInteiro("a categoria para editar: ");
                if (categoria > 7 || categoria < 0) {
                    mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!\n");
                }
            } while (categoria > 7 || categoria < 0);

            switch (categoria) {
                case 1:
                    newtitulo = lerString("o novo título:");
                    res.setTitulo(newtitulo);
                    mensagem(res.toString() + "\n");
                    break;

                case 2:
                    do {
                        mensagem("[1] Sob Avaliação\n[2] Aprovado\n[3] Reprovado\n");
                        newsitu = lerInteiro("a situação: ");
                        switch (newsitu) {
                            case 1:
                                newsituacao = Situacao.SOBAVALIACAO;
                                res.setSituacao(newsituacao);
                                break;

                            case 2:
                                newsituacao = Situacao.APROVADO;
                                res.setSituacao(newsituacao);
                                break;

                            case 3:
                                newsituacao = Situacao.REPROVADO;
                                res.setSituacao(newsituacao);
                                break;

                            default:
                                mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!");
                                break;
                        }
                    } while (newsitu <= 0 || newsitu > 3);
                    mensagem(res.toString() + "\n");
                    break;

                case 3:
                    String show = "";
                    int respostaAutor,
                     cont = 0;
                    List<String> newautor = res.getAutor();
                    for (int i = 0; i < res.getAutor().size(); i++) {
                        show += "[" + (i + 1) + "] " + res.getAutor().get(i) + "\n";
                        cont++;
                    }

                    mensagem("\n" + show);
                    respostaAutor = lerInteiro("o número que você deseja editar:");
                    if (respostaAutor <= res.getAutor().size()) {
                        switch (respostaAutor) {
                            case 1:
                                newautor.remove(0);
                                newautor.add(0, lerString("o novo autor: "));
                                break;

                            case 2:
                                newautor.remove(1);
                                newautor.add(1, lerString("o novo autor: "));
                                break;

                            case 3:
                                newautor.remove(2);
                                newautor.add(2, lerString("o novo autor: "));
                                break;

                            case 4:
                                newautor.remove(3);
                                newautor.add(3, lerString("o novo autor: "));
                                break;

                            case 5:
                                newautor.remove(4);
                                newautor.add(4, lerString("o novo autor: "));
                                break;

                            case 6:
                                newautor.remove(5);
                                newautor.add(5, lerString("o novo autor: "));
                                break;

                            case 7:
                                newautor.remove(6);
                                newautor.add(6, lerString("o novo autor: "));
                                break;

                            case 8:
                                newautor.remove(7);
                                newautor.add(7, lerString("o novo autor: "));
                                break;
                        }
                    }
                    res.setAutor(newautor);
                    mensagem(res.toString() + "\n");
                    break;

                case 4:
                    String showInst = "";
                    int respostaInst,
                     cont2 = 0;
                    List<String> newInst = res.getInstituicao();
                    for (int i = 0; i < res.getInstituicao().size(); i++) {
                        showInst += "[" + (i + 1) + "] " + res.getInstituicao().get(i) + "\n";
                        cont2++;
                    }

                    mensagem("\n" + showInst);
                    respostaInst = lerInteiro("o número que você deseja editar:");
                    if (respostaInst <= cont2) {
                        switch (respostaInst) {
                            case 1:
                                newInst.remove(0);
                                newInst.add(0, lerString("a nova isntituição: "));
                                break;

                            case 2:
                                newInst.remove(1);
                                newInst.add(1, lerString("a nova isntituição: "));
                                break;

                            case 3:
                                newInst.remove(2);
                                newInst.add(2, lerString("a nova isntituição: "));
                                break;

                            case 4:
                                newInst.remove(3);
                                newInst.add(3, lerString("a nova isntituição: "));
                                break;

                            case 5:
                                newInst.remove(4);
                                newInst.add(4, lerString("a nova isntituição: "));
                                break;

                            case 6:
                                newInst.remove(5);
                                newInst.add(5, lerString("a nova isntituição: "));
                                break;

                            case 7:
                                newInst.remove(6);
                                newInst.add(6, lerString("a nova isntituição: "));
                                break;

                            case 8:
                                newInst.remove(7);
                                newInst.add(7, lerString("a nova isntituição: "));
                                break;
                        }
                    }
                    res.setInstituicao(newInst);
                    mensagem(res.toString() + "\n");
                    break;

                case 5:
                    String showpc = "";
                    int respostapc,
                     cont3 = 0;
                    List<String> newpc = res.getPalavraChave();
                    for (int i = 0; i < res.getPalavraChave().size(); i++) {
                        showpc += "[" + (i + 1) + "] " + res.getPalavraChave().get(i) + "\n";
                        cont3++;
                    }

                    mensagem("\n" + showpc);
                    respostapc = lerInteiro("o número que você deseja editar:");
                    if (respostapc <= cont3) {
                        switch (respostapc) {
                            case 1:
                                newpc.remove(0);
                                newpc.add(0, lerString("a nova palavra chave: "));
                                break;

                            case 2:
                                newpc.remove(1);
                                newpc.add(1, lerString("a nova palavra chave: "));
                                break;

                            case 3:
                                newpc.remove(2);
                                newpc.add(2, lerString("a nova palavra chave: "));
                                break;

                            case 4:
                                newpc.remove(3);
                                newpc.add(3, lerString("a nova palavra chave: "));
                                break;
                        }
                    } else {
                        mensagem("é no if");
                    }
                    res.setPalavraChave(newpc);
                    mensagem(res.toString() + "\n");
                    break;

                case 0:
                    mensagem("\n");
                    break;
            }

        }
        return true;
    }
    */

    /**
     * Verifica se existem autores ou titulos iguais aos que o usuário digitou e
     * retorna true se a pesquisa foi bem sucedida ou false caso contrario.
     *
     * @return
     */
    public static boolean pesquisar() {
        Resumos res = new Resumos();
        mensagem("[1]Pesquisar por título\n[2]Pesquisar por autor\n");
        int opcao = lerInteiro("sua opção:\n");
        String pesquisa = lerString("a pesquisa:");
        switch (opcao) {
            case 1:
                Submissao sub = res.consultarTitulo(pesquisa);
                if (sub == null) {
                    System.out.println("NENHUM RESULTADO FOI ENCONTRADO!");
                    return false;
                } else {
                    mensagem(sub.toString());
                }
                break;

            case 2:
                List<Submissao> submissao = res.consultarAutor(pesquisa);
                if (submissao.isEmpty()) {
                    mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n");
                    return false;
                } else {
                    for (Submissao autores : submissao) {
                        mensagem(autores.toString() + "\n");
                    }
                }
                break;
        }
        return true;
    }
    
    /**
     * Método que percorre a lista de objetos procurando submissoes que tenham o mesmo autor
     * passado por parametro, caso encontrou lista e retorna uma list sub.
     * @param autor
     * @return 
     */
    @Override
     public List<Submissao> consultarAutor(String autor) {
        List<Submissao> sub = new ArrayList();
        for (Submissao categoria: ListaResumo) {
            for (int i=0; i<categoria.getAutor().size(); i++) {
                if (categoria.getAutor().get(i).equalsIgnoreCase(autor)) {
                    sub.add(categoria);
                }
            }
        }
        return sub;
    }
   
    /*
    public List<Submissao> consultarAutor(String autor) {
        List<Submissao> sub = new ArrayList();
        for (Submissao categoria : ListaResumo) {
            for (int i = 0; i < categoria.getAutor().size(); i++) {
                if (categoria.getAutor().get(i).toLowerCase().contains(autor.toLowerCase())) {
                    sub.add(categoria);
                }
            }
        }
        return sub;
    }
    */
    
    /**
     * Método que recebe um titulo e caso encontrou uma Submissao com esse titulo retorna o mesmo. 
     * @param titulo
     * @return 
     */
     @Override
    public Submissao consultarTitulo(String titulo) {
        Submissao sub = null;
        for (Submissao categoria: ListaResumo) {
            if (categoria.getTitulo().equalsIgnoreCase(titulo)) {
                sub = categoria;
            }
        }
        return sub;
    }
    /*
    public Submissao consultarTitulo(String titulo) {
        Submissao sub = null;
        for (Submissao categoria : ListaResumo) {
            if (categoria.getTitulo().contains(titulo)) {
                sub = categoria;
            }
        }
        return sub;
    }
    */

    /**
     * Método que exclui algum objeto selecionado através de uma pesquisa pelo
     * titulo.
     *
     * @param titulo
     * @return
     */
     @Override
    public boolean excluir(String titulo) {
        Resumos res = new Resumos();
        Submissao sub = res.consultarTitulo(titulo);
        if (sub==null) {
            return false;
        } else {
            ListaResumo.remove(sub);
        }
        return true;
    }
    /*
    @Override
    public boolean excluir(String titulo) {
        Resumos resu = new Resumos();
        Submissao sub = resu.consultarTitulo(titulo);

        if (sub == null) {
            System.out.println("NENHUM RESULTADO FOI ENCONTRADO!");
            return false;
        } else {
            System.out.println(sub.toString());
            String resposta = lerString("se deseja excluir (s/n): ");
            if (resposta.equals("s")) {
                ListaResumo.remove(sub);
            } else {
                mensagem("\n");
            }
        }

        return true;
    }*/

}