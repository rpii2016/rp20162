package resumo;

import java.util.List;
import rp2.Situacao;
import rp2.SubmissaoCientifica;



/**
 * Classe com tipos abstratos de dados herdados da classe SubmissaoCientifica para objetos do tipo Resumo, onde serão contidos valores e métodos para o mesmo.
 * @author Huillian Serpa
 *
 */

public class Resumo extends SubmissaoCientifica{
    
    /**
     * Construtor Classe Resumo que estende atributos da classe SubmissaoCientifica
     * @param titulo
     * @param situacao
     * @param autores
     * @param maxAutores
     * @param instituicao
     * @param palavraChave
     * @param maxInst
     * @param maxPalavras 
     */
    public Resumo(String titulo, Situacao situacao, List<String> autores, int maxAutores, List<String> instituicao, List<String> palavraChave, int maxInst, int maxPalavras) {
        super(titulo, situacao, autores, maxAutores, instituicao, palavraChave, maxInst, maxPalavras);
    }

    /**
     *
     * @return
     */
    @Override
    public String toString(){
        return super.toString();
    }    
}