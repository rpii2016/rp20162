package rp2;

import java.util.ArrayList;
import java.util.List;
import artigo.Artigo;
import artigo.Artigos;
import minicurso.Minicurso;
import minicurso.Minicursos;
import resumo.Resumo;
import resumo.Resumos;
import palestra.Palestra;
import palestra.Palestras;
import static rp2.Base.lerInteiro;
import static rp2.Base.lerString;
import static rp2.Base.mensagem;

/**
 * Classe para estabelecer as interações entre o sistema e o usuario.
 * @author Bruno
 */
public class Interacao {


    /**
     * Método que cria um novo objeto Submissao Cientifica com todos os parametros preenchidos.
     * @return sub Retorna o objeto criado. 
     */
    public SubmissaoCientifica incluirCientifica() {
        String titulo;
        int autores, palavras, situacaoEscolha;
        Situacao situacao = null;
        ArrayList autor = new ArrayList();
        ArrayList instituicao = new ArrayList();
        ArrayList palavraChave = new ArrayList();

        mensagem("\n");
        titulo = lerString("o título: ");

        do {
            mensagem("[1] Sob Avaliação\n[2] Aprovado\n[3] Reprovado\n");
            situacaoEscolha = lerInteiro("a situação: ");
            switch (situacaoEscolha) {
                case 1:
                    situacao = Situacao.SOBAVALIACAO;
                    break;

                case 2:
                    situacao = Situacao.APROVADO;
                    break;

                case 3:
                    situacao = Situacao.REPROVADO;
                    break;

                default:
                    mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!\n");
                    break;
            }
        } while (situacaoEscolha <= 0 || situacaoEscolha > 3);

        /*==============================================================================*/
        do {
            autores = lerInteiro("o número de autores que voce deseja inserir:");
            if (autores <= 0 || autores > 8) {
                mensagem("VOCE DEVE ADICIONAR ENTRE 1 A 8 AUTORES!\n\n");
            }
        } while (autores <= 0 || autores > 8);

        for (int i = 0; i < autores; i++) {
            autor.add(lerString("o " + (i + 1) + "º autor: "));
        }

        /*==============================================================================*/
        for (int x = 0; x < autores; x++) {
            instituicao.add(lerString("a instituição referente ao " + autor.get(x) + ": "));
        }

        /*=============================================================================*/
        do {
            palavras = lerInteiro("o número de palavras-chave que você deseja inserir: ");
            if (palavras <= 0 || palavras > 4) {
                mensagem("VOCE ADICIONAR ENTRE 1 A 4 PALAVRAS-CHAVE!\n\n");
            }
        } while (palavras <= 0 || palavras > 4);

        for (int z = 0; z < palavras; z++) {
            palavraChave.add(lerString("a " + (z + 1) + "º palavra-chave: "));
        }

        SubmissaoCientifica sub = new SubmissaoCientifica(titulo, situacao, autor, autores, instituicao, palavraChave, palavras, palavras) {};

        return sub;
    }

    /**
     * Pesquisa por titulo um objeto para ser editado
     * @return objeto artigo com todas as mudanças feitas
     */
    public Artigo editarArtigo() {
        Artigos newArtigos = new Artigos();
        String pesquisa = lerString("um titulo para editar:");
        Submissao sub = newArtigos.consultarTitulo(pesquisa);
        Artigo art = (Artigo) sub;

        if (sub == null) {
            mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n\n");
        } else {
            mensagem(sub.toString());

            String newtitulo, newresumo, newabstract;
            Situacao newsituacao;
            int categoria, newescolha;
            do {
                mensagem("\n[1] Título\n[2] Situação\n[3] Autor\n[4] Instituição\n[5] Palavra-Chave\n[6] Resumo\n[7] Abstract\n");
                categoria = lerInteiro("a categoria para editar: ");
                if (categoria > 7 || categoria <= 0) {
                    mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!\n");
                }
            } while (categoria > 7 || categoria <= 0);

            switch (categoria) {
                case 1:
                    newtitulo = lerString("o novo título:");
                    art.setTitulo(newtitulo);
                    mensagem(art.toString());
                    break;

                case 2:
                    do {
                        mensagem("[1] Sob Avaliação\n[2] Aprovado\n[3] Reprovado\n");
                        newescolha = lerInteiro("a situação: ");
                        switch (newescolha) {
                            case 1:
                                newsituacao = Situacao.SOBAVALIACAO;
                                art.setSituacao(newsituacao);
                                break;

                            case 2:
                                newsituacao = Situacao.APROVADO;
                                art.setSituacao(newsituacao);
                                break;

                            case 3:
                                newsituacao = Situacao.REPROVADO;
                                art.setSituacao(newsituacao);
                                break;

                            default:
                                mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!");
                                break;
                        }
                    } while (newescolha <= 0 || newescolha > 3);
                    mensagem(art.toString());
                    break;

                case 3:
                    String show = "";
                    int respostaAutor,
                     cont = 0;
                    List<String> newautor = art.getAutor();
                    for (int i = 0; i < art.getAutor().size(); i++) {
                        show += "[" + (i + 1) + "] " + art.getAutor().get(i) + "\n";
                        cont++;
                    }

                    mensagem("\n" + show);
                    respostaAutor = lerInteiro("o número que você deseja editar:");
                    if (respostaAutor <= art.getAutor().size()) {
                        switch (respostaAutor) {
                            case 1:
                                newautor.remove(0);
                                newautor.add(0, lerString("o novo autor: "));
                                break;

                            case 2:
                                newautor.remove(1);
                                newautor.add(1, lerString("o novo autor: "));
                                break;

                            case 3:
                                newautor.remove(2);
                                newautor.add(2, lerString("o novo autor: "));
                                break;

                            case 4:
                                newautor.remove(3);
                                newautor.add(3, lerString("o novo autor: "));
                                break;

                            case 5:
                                newautor.remove(4);
                                newautor.add(4, lerString("o novo autor: "));
                                break;

                            case 6:
                                newautor.remove(5);
                                newautor.add(5, lerString("o novo autor: "));
                                break;

                            case 7:
                                newautor.remove(6);
                                newautor.add(6, lerString("o novo autor: "));
                                break;

                            case 8:
                                newautor.remove(7);
                                newautor.add(7, lerString("o novo autor: "));
                                break;
                        }
                    }
                    art.setAutor(newautor);
                    mensagem(art.toString());
                    break;

                case 4:
                    String showInst = "";
                    int respostaInst,
                     cont2 = 0;
                    List<String> newInst = art.getInstituicao();
                    for (int i = 0; i < art.getInstituicao().size(); i++) {
                        showInst += "[" + (i + 1) + "] " + art.getInstituicao().get(i) + "\n";
                        cont2++;
                    }

                    mensagem("\n" + showInst);
                    respostaInst = lerInteiro("o número que você deseja editar:");
                    if (respostaInst <= cont2) {
                        switch (respostaInst) {
                            case 1:
                                newInst.remove(0);
                                newInst.add(0, lerString("a nova isntituição: "));
                                break;

                            case 2:
                                newInst.remove(1);
                                newInst.add(1, lerString("a nova isntituição: "));
                                break;

                            case 3:
                                newInst.remove(2);
                                newInst.add(2, lerString("a nova isntituição: "));
                                break;

                            case 4:
                                newInst.remove(3);
                                newInst.add(3, lerString("a nova isntituição: "));
                                break;

                            case 5:
                                newInst.remove(4);
                                newInst.add(4, lerString("a nova isntituição: "));
                                break;

                            case 6:
                                newInst.remove(5);
                                newInst.add(5, lerString("a nova isntituição: "));
                                break;

                            case 7:
                                newInst.remove(6);
                                newInst.add(6, lerString("a nova isntituição: "));
                                break;

                            case 8:
                                newInst.remove(7);
                                newInst.add(7, lerString("a nova isntituição: "));
                                break;
                        }
                    }
                    art.setInstituicao(newInst);
                    mensagem(art.toString());
                    break;

                case 5:
                    String showWord = "";
                    int respostaWord,
                     cont3 = 0;
                    List<String> newWord = art.getPalavraChave();
                    for (int i = 0; i < art.getPalavraChave().size(); i++) {
                        showWord += "[" + (i + 1) + "] " + art.getPalavraChave().get(i) + "\n";
                        cont3++;
                    }

                    mensagem("\n" + showWord);
                    respostaWord = lerInteiro("o número que você deseja editar:");
                    if (respostaWord <= cont3) {
                        switch (respostaWord) {
                            case 1:
                                newWord.remove(0);
                                newWord.add(0, lerString("a nova palavra chave: "));
                                break;

                            case 2:
                                newWord.remove(1);
                                newWord.add(1, lerString("a nova palavra chave: "));
                                break;

                            case 3:
                                newWord.remove(2);
                                newWord.add(2, lerString("a nova palavra chave: "));
                                break;

                            case 4:
                                newWord.remove(3);
                                newWord.add(3, lerString("a nova palavra chave: "));
                                break;
                        }
                    }
                    art.setPalavraChave(newWord);
                    mensagem(art.toString());
                    break;

                case 6:
                    newresumo = lerString("o novo resumo:");
                    art.setResumo(newresumo);
                    mensagem(art.toString());
                    break;

                case 7:
                    newabstract = lerString("o novo abstract:");
                    art.setAbstractt(newabstract);
                    mensagem(art.toString());
                    break;
            }

        }
        return art;
    }

    /**
     * Método que lê a pesquisa por autor ou por titulo digitada pelo usuario e mostra o que foi encontrado
     * Caso não encontrar nada mostra um mensagem de erro.
     */
    public void pesquisarArtigo() {
        Artigos newArtigos = new Artigos();
        mensagem("\n[1]Pesquisar por título\n[2]Pesquisar por autor\n");
        int opcao1 = lerInteiro("sua opção:");
        String pesquisa = lerString("a pesquisa:");

        switch (opcao1) {
            case 1:
                Submissao sub = newArtigos.consultarTitulo(pesquisa);
                if (sub == null) {
                    mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n\n");
                } else {
                    mensagem(sub.toString() + "\n");
                }
                break;

            case 2:
                List<Submissao> submissao = newArtigos.consultarAutor(pesquisa);
                if (submissao.isEmpty()) {
                    mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n\n");

                } else {
                    for (Submissao autores : submissao) {
                        mensagem(autores.toString() + "\n");
                    }
                }
                break;
        }
    }

    /**
     * Método para pesquisar um determinado objeto Artigo e exclui-lo
     * Caso não encontre nenhum objeto uma mensagem é apresentada
     */
    public void excluirArtigo() {
        Artigos newArtigos = new Artigos();
        String pesquisaArtigo = lerString("um titulo para excluir:");
        Submissao subArtigo = newArtigos.consultarTitulo(pesquisaArtigo);

        if (newArtigos.excluir(pesquisaArtigo) == false) {
            mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n\n");
        } else {
            mensagem(subArtigo.toString() + "A EXCLUSÃO FOI FEITA COM SUCESSO!\n\n");
        }
    }

      /**
     * Pesquisa por titulo um objeto para ser editado
     * @return objeto Resumo com todas as mudanças feitas
     */
    public Resumo editarResumo() {
        Resumos res = new Resumos();
        String pesquisa = lerString("um titulo para editar:");
        Submissao sub = res.consultarTitulo(pesquisa);
        Resumo resu = (Resumo) sub;

        if (sub == null) {
            mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n\n");
        } else {
            mensagem(sub.toString());

            String newtitulo;
            Situacao newsituacao;
            int categoria, newescolha;
            do {
                mensagem("\n[1] Título\n[2] Situação\n[3] Autor\n[4] Instituição\n[5] Palavra-Chave\n");
                categoria = lerInteiro("a categoria para editar: ");
                if (categoria > 7 || categoria <= 0) {
                    mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!\n");
                }
            } while (categoria > 7 || categoria <= 0);

            switch (categoria) {
                case 1:
                    newtitulo = lerString("o novo título:");
                    resu.setTitulo(newtitulo);
                    mensagem(resu.toString());
                    break;

                case 2:
                    do {
                        mensagem("[1] Sob Avaliação\n[2] Aprovado\n[3] Reprovado\n");
                        newescolha = lerInteiro("a situação: ");
                        switch (newescolha) {
                            case 1:
                                newsituacao = Situacao.SOBAVALIACAO;
                                resu.setSituacao(newsituacao);
                                break;

                            case 2:
                                newsituacao = Situacao.APROVADO;
                                resu.setSituacao(newsituacao);
                                break;

                            case 3:
                                newsituacao = Situacao.REPROVADO;
                                resu.setSituacao(newsituacao);
                                break;

                            default:
                                mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!");
                                break;
                        }
                    } while (newescolha <= 0 || newescolha > 3);
                    mensagem(resu.toString());
                    break;

                case 3:
                    String show = "";
                    int respostaAutor,
                     cont = 0;
                    List<String> newautor = resu.getAutor();
                    for (int i = 0; i < resu.getAutor().size(); i++) {
                        show += "[" + (i + 1) + "] " + resu.getAutor().get(i) + "\n";
                        cont++;
                    }

                    mensagem("\n" + show);
                    respostaAutor = lerInteiro("o número que você deseja editar:");
                    if (respostaAutor <= resu.getAutor().size()) {
                        switch (respostaAutor) {
                            case 1:
                                newautor.remove(0);
                                newautor.add(0, lerString("o novo autor: "));
                                break;

                            case 2:
                                newautor.remove(1);
                                newautor.add(1, lerString("o novo autor: "));
                                break;

                            case 3:
                                newautor.remove(2);
                                newautor.add(2, lerString("o novo autor: "));
                                break;

                            case 4:
                                newautor.remove(3);
                                newautor.add(3, lerString("o novo autor: "));
                                break;

                            case 5:
                                newautor.remove(4);
                                newautor.add(4, lerString("o novo autor: "));
                                break;

                            case 6:
                                newautor.remove(5);
                                newautor.add(5, lerString("o novo autor: "));
                                break;

                            case 7:
                                newautor.remove(6);
                                newautor.add(6, lerString("o novo autor: "));
                                break;

                            case 8:
                                newautor.remove(7);
                                newautor.add(7, lerString("o novo autor: "));
                                break;
                        }
                    }
                    resu.setAutor(newautor);
                    mensagem(resu.toString());
                    break;

                case 4:
                    String showInst = "";
                    int respostaInst,
                     cont2 = 0;
                    List<String> newInst = resu.getInstituicao();
                    for (int i = 0; i < resu.getInstituicao().size(); i++) {
                        showInst += "[" + (i + 1) + "] " + resu.getInstituicao().get(i) + "\n";
                        cont2++;
                    }

                    mensagem("\n" + showInst);
                    respostaInst = lerInteiro("o número que você deseja editar:");
                    if (respostaInst <= cont2) {
                        switch (respostaInst) {
                            case 1:
                                newInst.remove(0);
                                newInst.add(0, lerString("a nova isntituição: "));
                                break;

                            case 2:
                                newInst.remove(1);
                                newInst.add(1, lerString("a nova isntituição: "));
                                break;

                            case 3:
                                newInst.remove(2);
                                newInst.add(2, lerString("a nova isntituição: "));
                                break;

                            case 4:
                                newInst.remove(3);
                                newInst.add(3, lerString("a nova isntituição: "));
                                break;

                            case 5:
                                newInst.remove(4);
                                newInst.add(4, lerString("a nova isntituição: "));
                                break;

                            case 6:
                                newInst.remove(5);
                                newInst.add(5, lerString("a nova isntituição: "));
                                break;

                            case 7:
                                newInst.remove(6);
                                newInst.add(6, lerString("a nova isntituição: "));
                                break;

                            case 8:
                                newInst.remove(7);
                                newInst.add(7, lerString("a nova isntituição: "));
                                break;
                        }
                    }
                    resu.setInstituicao(newInst);
                    mensagem(resu.toString());
                    break;

                case 5:
                    String showWord = "";
                    int respostaWord,
                     cont3 = 0;
                    List<String> newWord = resu.getPalavraChave();
                    for (int i = 0; i < resu.getPalavraChave().size(); i++) {
                        showWord += "[" + (i + 1) + "] " + resu.getPalavraChave().get(i) + "\n";
                        cont3++;
                    }

                    mensagem("\n" + showWord);
                    respostaWord = lerInteiro("o número que você deseja editar:");
                    if (respostaWord <= cont3) {
                        switch (respostaWord) {
                            case 1:
                                newWord.remove(0);
                                newWord.add(0, lerString("a nova palavra chave: "));
                                break;

                            case 2:
                                newWord.remove(1);
                                newWord.add(1, lerString("a nova palavra chave: "));
                                break;

                            case 3:
                                newWord.remove(2);
                                newWord.add(2, lerString("a nova palavra chave: "));
                                break;

                            case 4:
                                newWord.remove(3);
                                newWord.add(3, lerString("a nova palavra chave: "));
                                break;
                        }
                    }
                    resu.setPalavraChave(newWord);
                    mensagem(resu.toString());
                    break;

            }

        }
        return resu;
    }

    /**
     * Método que lê a pesquisa por autor ou por titulo digitada pelo usuario e mostra o que foi encontrado
     * Caso não encontrar nada mostra um mensagem de erro.
     */
    public void pesquisarResumo() {
        Resumos res = new Resumos();
        mensagem("\n[1]Pesquisar por título\n[2]Pesquisar por autor\n");
        int opcao1 = lerInteiro("sua opção:");
        String pesquisa = lerString("a pesquisa:");

        switch (opcao1) {
            case 1:
                Submissao sub = res.consultarTitulo(pesquisa);
                if (sub == null) {
                    mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n\n");

                } else {
                    mensagem(sub.toString() + "\n");
                }
                break;

            case 2:
                List<Submissao> submissao = res.consultarAutor(pesquisa);
                if (submissao.isEmpty()) {
                    mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n\n");

                } else {
                    for (Submissao autores : submissao) {
                        mensagem(autores.toString() + "\n");
                    }
                }
                break;
        }
    }

    /**
     * Método para pesquisar um determinado objeto Resumo e exclui-lo
     * Caso não encontre nenhum objeto uma mensagem é apresentada
     */
    public void excluirResumo() {
        Resumos res = new Resumos();
        String pesquisaResumo = lerString("um titulo para excluir:");
        Submissao subResumo = res.consultarTitulo(pesquisaResumo);

        if (res.excluir(pesquisaResumo) == false) {
            mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n\n");
        } else {
            mensagem(subResumo.toString() + "A EXCLUSÃO FOI FEITA COM SUCESSO!\n\n");
        }
    }

    /**
     * Método que cria um objeto Submissao Cientifica com todos os parametros preenchidos.
     * @return sub Retorna o objeto criado 
     */
    public SubmissaoApresentacao incluirApresentacao(String secao) {
        String titulo, resumo, abstractt, curriculo = "";
        int autores = 0, situacaoEscolha, duracao = 0;
        Situacao situacao = null;
        ArrayList autor = new ArrayList();

        mensagem("\n");
        titulo = lerString("o título: ");

        do {
            mensagem("[1] Sob Avaliação\n[2] Aprovado\n[3] Reprovado\n");
            situacaoEscolha = lerInteiro("a situação: ");
            switch (situacaoEscolha) {
                case 1:
                    situacao = Situacao.SOBAVALIACAO;
                    break;

                case 2:
                    situacao = Situacao.APROVADO;
                    break;

                case 3:
                    situacao = Situacao.REPROVADO;
                    break;

                default:
                    mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!\n");
                    break;
            }
        } while (situacaoEscolha <= 0 || situacaoEscolha > 3);
        
        if (secao.equals("MINICURSO")) {
                    do {
                        autores = lerInteiro("o número de autores que voce deseja inserir:");
                        if (autores <= 0 || autores > 3) {
                            mensagem("VOCE DEVE ADICIONAR ENTRE 1 A 3 AUTORES!\n\n");
                        }
                    } while (autores <= 0 || autores > 3);

                    for (int i = 0; i < autores; i++) {
                        autor.add(lerString("o " + (i + 1) + "º autor: "));
                    }
        } else {
            autor.add(lerString("o autor: "));
        }

        resumo = lerString("o resumo:");
        abstractt = lerString("o abstract:");
        duracao = lerInteiro("a duração em minutos: ");

        SubmissaoApresentacao subApresentacao = new SubmissaoApresentacao(titulo, situacao, autor, autores, resumo, abstractt, duracao) {};
        
        return subApresentacao;
    }

      /**
     * Pesquisa por titulo um objeto para ser editado
     * @return objeto Palestra com todas as mudanças feitas
     */
    public Palestra editarPalestra() {

        Palestras newPalestras = new Palestras();
        String pesquisa = lerString("um titulo para editar:");
        Submissao sub = newPalestras.consultarTitulo(pesquisa);
        Palestra palestra = (Palestra) sub;
        if (sub == null) {
            mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n\n");
        } else {
            mensagem(sub.toString());
            String newtitulo, newresumo, newabstractt, newcurriculo;
            Situacao newsituacao;
            int categoria, newescolha, newduracao;
            do {
                mensagem("\n[1] Título\n[2] Situação\n[3] Autor\n[4] Resumo\n[5] Abstract\n[6] Duração\n[7] Curriculo\n[0] Sair\n");
                categoria = lerInteiro("a categoria para editar: ");
                if (categoria > 7 || categoria < 0) {
                    mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!\n");
                }
            } while (categoria > 7 || categoria < 0);

            switch (categoria) {
                case 1:
                    newtitulo = lerString("o novo título:");
                    palestra.setTitulo(newtitulo);
                    mensagem(palestra.toString() + "\n");
                    break;

                case 2:
                    do {
                        mensagem("[1] Sob Avaliação\n[2] Aprovado\n[3] Reprovado\n");
                        newescolha = lerInteiro("a situação: ");
                        switch (newescolha) {
                            case 1:
                                newsituacao = Situacao.SOBAVALIACAO;
                                palestra.setSituacao(newsituacao);
                                break;

                            case 2:
                                newsituacao = Situacao.APROVADO;
                                palestra.setSituacao(newsituacao);
                                break;

                            case 3:
                                newsituacao = Situacao.REPROVADO;
                                palestra.setSituacao(newsituacao);
                                break;

                            default:
                                mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!");
                                break;
                        }
                    } while (newescolha <= 0 || newescolha > 3);
                    mensagem(palestra.toString() + "\n");
                    break;

                case 3:
                    List<String> newautor = palestra.getAutor();
                    newautor.remove(0);
                    newautor.add(0, lerString("o novo autor: "));
                    mensagem(palestra.toString() + "\n");
                    break;

                case 4:
                    newresumo = lerString("o novo resumo:");
                    palestra.setResumo(newresumo);
                    mensagem(palestra.toString() + "\n");
                    break;               

                case 5:
                    newabstractt = lerString("o novo abstract:");
                    palestra.setAbstractt(newabstractt);
                    mensagem(palestra.toString() + "\n");
                    break;

                case 6:
                    newduracao = lerInteiro("a nova duracao:");
                    palestra.setDuracao(newduracao);
                    mensagem(palestra.toString() + "\n");
                    break;

                case 7:
                    newcurriculo = lerString("o novo curriculo:");
                    palestra.setCurriculo(newcurriculo);
                    mensagem(palestra.toString() + "\n");
                    break;

                case 0:
                    mensagem("\n");
                    break;
            }
        }
        return palestra;
    }
    
    /**
     * Método que lê a pesquisa por autor ou por titulo digitada pelo usuario e mostra o que foi encontrado
     * Caso não encontrar nada mostra um mensagem de erro.
     */
    public void pesquisarPalestra() {
        Palestras inter = new Palestras();
        mensagem("\n[1]Pesquisar por título: \n[2]Pesquisar por autor: \n");
        int opcao = lerInteiro("sua opção:");
        String pesquisa = lerString("a pesquisa:");

        switch (opcao) {
            case 1:
                Submissao sub = inter.consultarTitulo(pesquisa);
                if (sub == null) {
                    mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n");
                } else {
                    mensagem(sub.toString() + "\n");
                }
                break;

            case 2:
                List<Submissao> submissao = inter.consultarAutor(pesquisa);
                if (submissao.isEmpty()) {
                    mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n");
                } else {
                    for (Submissao autores : submissao) {
                        mensagem(autores.toString() + "\n");
                    }
                }
                break;
        }
    }
    
    /**
     * Método para pesquisar um determinado objeto Palestra e exclui-lo
     * Caso não encontre nenhum objeto uma mensagem é apresentada
     */
    public void excluirPalestra(){
       Palestras newPalestras = new Palestras();
       String titulo=lerString("o titulo para excluir: ");
        
        Submissao subPalestras = newPalestras.consultarTitulo(titulo);
        if (newPalestras.excluir(titulo) == false) {
            mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n\n");
        } else {
            mensagem(subPalestras.toString() + "A EXCLUSÃO FOI FEITA COM SUCESSO!\n\n");
        }
    }
    
     public void pesquisarMinicurso(){
        Minicursos newMinicursos = new Minicursos();
        mensagem("\n[1]Pesquisar por título\n[2]Pesquisar por autor\n");
        int opcao1 = lerInteiro("sua opção:");
        String pesquisa = lerString("a pesquisa:");

        switch (opcao1) {
            case 1:
                Submissao sub = newMinicursos.consultarTitulo(pesquisa);
                if (sub == null) {
                    mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n\n");
                } else {
                    mensagem(sub.toString() + "\n");
                }
                break;

            case 2:
                List<Submissao> submissao = newMinicursos.consultarAutor(pesquisa);
                if (submissao.isEmpty()) {
                    mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n\n");

                } else {
                    for (Submissao autores : submissao) {
                        mensagem(autores.toString() + "\n");
                    }
                }
                break;
        }
    }
    
    public Minicurso editarMinicurso(){
        Minicursos newMinicursos = new Minicursos();
        String pesquisa = lerString ("Um titulo para editar: ");
        Submissao sub = newMinicursos.consultarTitulo(pesquisa);
        Minicurso mini = (Minicurso) sub;
        
        if (sub == null) {
            mensagem("NENHUM MINICURSO CADASTRADO!\n\n");
        } else {
            mensagem(sub.toString());
           
            String newtitulo, newresumo, newabstract, newmetodologia, newrecursos;
            Situacao newsituacao;
            int categoria, newescolha, newduracao;
            
            do {
                mensagem("\n[1] Título\n[2] Situação\n[3] Autor\n[4] Resumo\n[5] Abstract\n[6] Duracao\n[7] Recursos\n[8] Metodologia\n[0] Sair\n");
                categoria = lerInteiro("a categoria para editar: ");
                if (categoria > 7 || categoria < 0) {
                    mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!\n");
                }
            } while (categoria > 7 || categoria < 0);

            switch (categoria) {
                case 1:
                    newtitulo = lerString("o novo título: ");
                    mini.setTitulo(newtitulo);
                    mensagem(mini.toString() + "\n");
                    break;

                case 2:
                    do {
                        mensagem("[1] Sob Avaliação\n[2] Aprovado\n[3] Reprovado\n");
                        newescolha = lerInteiro("a situação: ");
                        switch (newescolha) {
                            case 1:
                                newsituacao = Situacao.SOBAVALIACAO;
                                mini.setSituacao(newsituacao);
                                break;

                            case 2:
                                newsituacao = Situacao.APROVADO;
                                mini.setSituacao(newsituacao);
                                break;

                            case 3:
                                newsituacao = Situacao.REPROVADO;
                                mini.setSituacao(newsituacao);
                                break;

                            default:
                                mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!");
                                break;
                        }
                    } while (newescolha <= 0 || newescolha > 3);
                    mensagem(mini.toString() + "\n");
                    break;

                case 3:
                    String show = "";
                    int respostaAutor,
                     cont = 0;
                    List<String> newautor = mini.getAutor();
                    for (int i = 0; i < mini.getAutor().size(); i++) {
                        show += "[" + (i + 1) + "] " + mini.getAutor().get(i) + "\n";
                        cont++;
                    }

                    mensagem("\n" + show);
                    respostaAutor = lerInteiro("o número de autores que você deseja editar:");
                    if (respostaAutor <= mini.getAutor().size()) {
                        switch (respostaAutor) {
                            case 1:
                                newautor.remove(0);
                                newautor.add(0, lerString("o novo autor: "));
                                break;

                            case 2:
                                newautor.remove(1);
                                newautor.add(1, lerString("o novo autor: "));
                                break;

                            case 3:
                                newautor.remove(2);
                                newautor.add(2, lerString("o novo autor: "));
                                break;
                        }
                    }
                    mini.setAutor(newautor);
                    mensagem(mini.toString() + "\n");
                    break;

                case 4:
                    newresumo = lerString("o novo resumo:");
                    mini.setResumo(newresumo);
                    mensagem(mini.toString() + "\n");
                    break;

                case 5:
                    newabstract = lerString("o novo abstract:");
                    mini.setAbstractt(newabstract);
                    mensagem(mini.toString() + "\n");
                    break;

                case 6:
                    newduracao = lerInteiro("a nova duracao:");
                    mini.setDuracao(newduracao);
                    mensagem(mini.toString() + "\n");
                    break;

                case 7:
                    newrecursos = lerString("os novos recursos:");
                    mini.setRecursos(newrecursos);
                    mensagem(mini.toString() + "\n");
                    break;

                case 8:
                    newmetodologia = lerString("a nova metodologia:");
                    mini.setMetodologia(newmetodologia);
                    mensagem(mini.toString() + "\n");
                    break;

                case 0:
                    mensagem("\n");
                    break;
            }
        }
        return mini;
    }
    
    public void excluirMinicurso(){
        Minicursos newMinicursos = new Minicursos();
        String pesquisarMinicurso = lerString("um titulo para excluir:");
        Submissao subMinicurso = newMinicursos.consultarTitulo(pesquisarMinicurso);

        if (newMinicursos.excluir(pesquisarMinicurso) == false) {
            mensagem("NENHUM RESULTADO FOI ENCONTRADO!\n\n");
        } else {
            mensagem(subMinicurso.toString() + "\nA EXCLUSÃO FOI FEITA COM SUCESSO!\n\n");
        }
    }
}