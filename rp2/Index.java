package rp2;
import relatorio.*;
import static rp2.Base.*;

/**@author Grupo 06
 */
public class Index {

    public static void main(String[] args) {
        int escolha;
        
        FormRelatoriosTecnicos FormRelatorios = new FormRelatoriosTecnicos();
        
        mensagem("***Bem vindo ao MENU PRINCIPAL!***");

        do {
            do {
                mensagem("\n[1] Artigo\n[2] Minicurso\n[3] Monografia\n[4] Palestra\n[5] Resumo\n[6] Relatório Técnico\n[0] Sair\n");
                escolha = lerInteiro("uma categoria para trabalhar:");
                if (escolha < 0 || escolha > 6) {
                    mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!\n");
                }
            } while (escolha < 0 || escolha > 6);

            switch (escolha) {
                
                case 0:
                    System.exit(escolha);
                break;
                    
                case 1:
                    menu("ARTIGO");
                break;

                case 2:
                    menu("MINICURSO");
                break;

                case 3:
                    menu("MONOGRAFIA");
                break;

                case 4:
                    menu("PALESTRA");
                break;

                case 5:
                    menu("RESUMO");
                            
                break;
                    
                case 6:
                    FormRelatorios.setVisible(true);
                    FormRelatorios.setAlwaysOnTop(true);
                break;

            }
        } while (escolha != 0);
    }
}