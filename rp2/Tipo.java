/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rp2;

/**
 *Classe enum com constantes do tipo Tipo
 * @author Bruno, Gustavo, Mauricio, Eduardo e Julielen
 */
public enum Tipo {
    GRADUACAO(1), ESPECIALIZACAO(2), MESTRADO(3), DOUTORADO(4);
    
   private final int tipo;
   /**
    * Construtor Classe enum Tipo recebe um int referente ao tipo
    * @param tipo 
    */
   Tipo(int tipo){
       this.tipo=tipo;
   }
      /**
       * Retorna String com tipo formatado
       * @return 
       */
      public String getTipo(){
        switch (this) {
            case GRADUACAO: return "Graduação";

            case ESPECIALIZACAO: return "Especializaçao";

            case MESTRADO: return "Mestrado";

            case DOUTORADO: return "Doutorado";
            default: return "";

        }
    }
   
   
}
