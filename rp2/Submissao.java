package rp2;

import java.util.List;

/**
 *
 * @author Bruno, Gustavo, Mauricio, Eduardo , Julielen e Willian.
 */
public abstract class Submissao {
    
    protected String titulo;
    protected Situacao situacao;
    protected List<String> autor;
    protected final int MAX_AUTORES;
    

    public Submissao(String titulo, Situacao situacao, List<String> autores, int maxAutores){
        this.titulo = titulo;
        this.situacao = situacao;
        this.autor = autores;
        MAX_AUTORES = maxAutores;    
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }
    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the situacao
     */
    public Situacao getSituacao() {
        return situacao;
    }
    /**
     * @param situacao the situacao to set
     */
    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    /**
     * @return the autores
     */
    public List<String> getAutor() {
        return autor;
    }
    /**
     * Atribui a lista de autores.
     * @param autores the autores to set
     * @return True se a lista não excede ao tamanho mãximo de autores. False em caso contrario.
     */
    public boolean setAutor(List<String> autores) {
        if(autores.size()<= MAX_AUTORES){
            this.autor = autores;
            return true;
        }       
        return  false;
    }

    /**
     * @return the MAX_AUTORES
     */
    public int getMAX_AUTORES() {
        return MAX_AUTORES;
    }
    
   /**
     * Adiciona um autor a submissao
     * @param autor autor a ser adicionado
     * @return verdadeiro se for adicionado um autor
     */
    public boolean addAutor(String autor){
        if(this.autor.size()< MAX_AUTORES){
            this.autor.add(autor);
            return true;
        }
        return false;
    }
    
    /**
     * verifica se um autor esta  na submissao
     * @param procuraAutor - autor a ser procurado
     * @return verdadeiro se o autor estiver na submissão
     */
    public boolean isAutor(String procuraAutor){
        for(String autor: autor){
            if(autor.equalsIgnoreCase(procuraAutor))
                return true;
        }
        return false;
    }
    
    @Override
    public String toString(){
        String dados = "\n";
        dados += "Título: "  + titulo +"\n" ;
        dados += "Situação: "  + situacao.getDescricao() + "\n";
        for(int i=0; i<autor.size(); i++){
            dados += (i+1) + "º Autor: "  + autor.get(i)+"\n";
        }
        return dados;
    }
}