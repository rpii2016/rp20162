package rp2;

import java.util.List;

/**
 *
 * @author Bruno, Gustavo, Mauricio, Eduardo , Julielen e Willian
 */
public abstract class SubmissaoApresentacao extends Submissao{

    protected String resumo;
    protected String abstractt;
    protected int duracao;
    
    public SubmissaoApresentacao (String titulo, Situacao situacao, List<String> autores, int maxAutores,String resumo, String abstractt, int duracao){
        super (titulo, situacao, autores, maxAutores);
        this.resumo = resumo;
        this.abstractt = abstractt;
        this.duracao = duracao;
    }

    /**
     * @return the resumo
     */
    public String getResumo() {
        return resumo;
    }
    /**
     * @param resumo the resumo to set
     */
    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    /**
     * @return the abstractt
     */
    public String getAbstractt() {
        return abstractt;
    }
    /**
     * @param abstractt the abstractt to set
     */
    public void setAbstractt(String abstractt) {
        this.abstractt = abstractt;
    }

    /**
     * @return the duracao
     */
    public int getDuracao() {
        return duracao;
    }
    /**
     * @param duracao the duracao to set
     */
    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }
    
        @Override
    public String toString(){
        String dados = super.toString();
        dados += "Resumo: "  + resumo ;
        dados += "\nAbstract: "  + abstractt;
        dados += "\nDuração: "  + duracao;
        return dados;
    }
}