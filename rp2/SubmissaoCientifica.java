package rp2;

import java.util.*;

/**
 *
 * @author Bruno, Gustavo, Mauricio, Eduardo , Julielen e Willian
 */
public abstract class SubmissaoCientifica extends Submissao {
    
    protected List<String> instituicao;
    protected List<String> palavraChave;
    protected final int MAX_INST;
    protected final int MAX_PALAVRAS;
    
    public SubmissaoCientifica (String titulo, Situacao situacao, List<String> autores, int maxAutores, 
    List<String> instituicao, List<String> palavraChave, int maxInst, int maxPalavras){
        super (titulo, situacao, autores, maxAutores);
        this.instituicao = instituicao;
        this.palavraChave = palavraChave;
        MAX_INST = maxInst;          
        MAX_PALAVRAS = maxPalavras;   
    }

    /**
     * @return the instituicao
     */
    public List<String> getInstituicao() {
        return instituicao;
    }
    /**
     * @param instituicao the instituicao to set
     */
    public void setInstituicao(List<String> instituicao) {
        this.instituicao = instituicao;
    }

    /**
     * @return the palavraChave
     */
    public List<String> getPalavraChave() {
        return palavraChave;
    }
    /**
     * @param palavraChave the palavraChave to set
     */
    public void setPalavraChave(List<String> palavraChave) {
        this.palavraChave = palavraChave;
    }

    /**
     * @return the MAX_INST
     */
    public int getMAX_INST() {
        return MAX_INST;
    }
    /**
     * @return the MAX_PALAVRAS
     */
    public int getMAX_PALAVRAS() {
        return MAX_PALAVRAS;
    }

       /**
     * Adiciona uma instituicao a submissao
     * @param inst - instituição a ser adicionada.
     * @return verdadeiro se adição tiver sucesso.
     */
    public boolean addInstituicao(String inst){
        if(this.instituicao.size()< MAX_INST){
            this.instituicao.add(inst);
            return true;
        }
        return false;
    }
       /**
     * Adiciona uma palavra chave a submissao
     * @param palavra - palavra a ser adicionada.
     * @return verdadeiro se a adição tiver sucesso
     */
    public boolean addPalavras(String palavra){
        if(this.palavraChave.size()< MAX_PALAVRAS){
            this.palavraChave.add(palavra);
            return true;
        }
        return false;
    }

        @Override
    public String toString(){
        String dados = super.toString();
        for(int i=0; i<instituicao.size(); i++)
            dados += (i+1) + "º Instituição: "  + instituicao.get(i) + "\n";
        
        for(int i=0; i<palavraChave.size(); i++)
            dados += (i+1) + "º Palavra-Chave: "  + palavraChave.get(i) + "\n";
        
        return dados;
    }
}