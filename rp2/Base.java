package rp2;

import java.util.Scanner;
import artigo.Artigo;
import artigo.Artigos;
import minicurso.Minicurso;
import minicurso.Minicursos;
import monografia.MonografiaInterface;
import palestra.Palestra;
import palestra.Palestras;
import resumo.Resumo;
import resumo.Resumos;

/**
 * Classe que contém os métodos que serão utilizados por todas as categorias
 *
 * @author Bruno, Gustavo, Mauricio, Eduardo e Julielen
 */
public class Base {

    /**
     * Método para passar todo o texto inserido a um system out print
     * @param texto 
     */
    public static void mensagem(String texto) {
        System.out.print(texto);
    }

    /**
     * Recebe o nome do campo desejado, e pede para o usuário digitar uma string
     * que não seja nula, a função se repete aé ser digitado um valor não nulo.
     *
     * @param nome - o nome do campo desejado
     * @return var - a String validada
     */
    public static String lerString(String nome) {
        Scanner entrada = new Scanner(System.in);
        String var;
        do {
            mensagem("Digite " + nome);
            var = entrada.nextLine();
        } while (var.trim().equalsIgnoreCase(""));
        return var;

    }

    /**
     * Função que testa se uma String pode ser convertida para um número inteiro
     * válido
     *
     * @param str
     * @return boolean
     */
    public static boolean isInteger(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    /**
     * Recebe o nome do campo desejado, e pede para o usuario digitar um
     * numero inteiro ate ele digitar um valor valido.
     *
     * @param nome - o nome do campo
     * @return integer - o numero inteiro valido
     */
    public static int lerInteiro(String nome) {
        Scanner entrada = new Scanner(System.in);
        String var;
        int integer;
        do {
            mensagem("Digite " + nome);
            var = entrada.nextLine().trim();
            if (isInteger(var) == false) {
                mensagem("VOCÊ DIGITOU UM VALOR INVÁLIDO!\n\n");
            }
        } while (isInteger(var) == false);
        integer = Integer.parseInt(var);
        return integer;
    }

    /**
     * Função que recebe o número de minutos em formato Inteiro, e retorna uma
     * String com o formato hh:mm
     *
     * @param minutos - Int com o valor de minutos
     * @return String - horas e minutos em formato hh:mm
     */
    public static String minPHoras(int minutos) {
        String horas;
        int h1, h2;
        h1 = minutos / 60;
        h2 = minutos % 60;
        horas = h1 + ":" + h2;
        if (h1 < 10) {
            horas = "0" + horas;
        }
        if (h2 == 0) {
            horas = horas + "0";
        }
        return horas;
    }

    /**
     * Metodo que exibe uma interface funcional com opções para o usuário
     * escolher
     *
     * @param secao escolhida pelo usuario
     */
    public static void menu(String secao) {
        int opcao;
        Interacao interacao = new Interacao();
        Artigos newArtigos = new Artigos();
        Minicursos newMinicursos = new Minicursos();
        Resumos newResumos = new Resumos();
        MonografiaInterface mono = new MonografiaInterface();
        Palestras newPalestras = new Palestras();
        Minicursos Min = new Minicursos();
        
        mensagem("\n***Bem vindo(a) a seção de " + secao + "***\n");
        do {
            do {
                mensagem("[1] Incluir\n[2] Consultar\n[3] Editar\n[4] Excluir\n[0] Sair\n");
                opcao = lerInteiro("uma opção:");
                if (opcao < 0 || opcao > 4) {
                    mensagem("Você digitou uma opção inválida.\n\n");
                }
            } while (opcao < 0 && opcao > 4);
            switch (opcao) {
                case 1:
                    switch (secao) {
                        case "ARTIGO":
                            SubmissaoCientifica newArt = interacao.incluirCientifica();
                            String resumo, abstrato;
                            resumo = lerString("o resumo: ");
                            abstrato = lerString("o abstract: ");

                            Artigo subArtigo = new Artigo(newArt.getTitulo(), newArt.getSituacao(), newArt.getAutor(), newArt.getMAX_AUTORES(),
                            newArt.getInstituicao(), newArt.getPalavraChave(), newArt.getMAX_INST(), newArt.getMAX_PALAVRAS(), resumo, abstrato);

                            if (newArtigos.incluir(subArtigo) == true) {
                                mensagem("A ADIÇÃO FOI FEITA COM SUCESSO!\n\n");
                            } else {
                                mensagem("\nERRO!\n");
                            }
                        break;

                        case "MINICURSO":
                            SubmissaoApresentacao newMini = interacao.incluirApresentacao(secao);
                            String recursos, metodologia;
                            recursos = lerString("os recursos: ");
                            metodologia = lerString("a metodologia: ");
                            
                            Minicurso subMinicurso = new Minicurso (newMini.getTitulo(), newMini.getSituacao(), newMini.getAutor(), newMini.MAX_AUTORES,
                            newMini.getResumo(), newMini.getAbstractt(), newMini.duracao, recursos, metodologia);
                            
                            if (newMinicursos.incluir(subMinicurso) == true) {
                                mensagem("A ADIÇÃO FOI FEITA COM SUCESSO!\n\n");
                            } else {
                                mensagem("\nERRO!\n");
                            }
                            break;

                        case "PALESTRA":
                            SubmissaoApresentacao newPalestra = interacao.incluirApresentacao(secao);
                            String curriculo = lerString("o curriculo: ");
                            
                            Palestra subPalestra = new Palestra(newPalestra.getTitulo(), newPalestra.getSituacao(), newPalestra.getAutor(),
                            newPalestra.getResumo(), newPalestra.getAbstractt(), newPalestra.getDuracao(), curriculo);
                            
                            if (newPalestras.incluir(subPalestra) == true) {
                                mensagem("A ADIÇÃO FOI FEITA COM SUCESSO!\n\n");
                            } else {
                                mensagem("\nERRO!\n");
                            }
                        break;

                        case "RESUMO":
                            SubmissaoCientifica newRes = interacao.incluirCientifica();

                            Resumo subResumo = new Resumo(newRes.getTitulo(), newRes.getSituacao(), newRes.getAutor(), newRes.getMAX_AUTORES(),
                            newRes.getInstituicao(), newRes.getPalavraChave(), newRes.getMAX_INST(), newRes.getMAX_PALAVRAS());

                            if (newResumos.incluir(subResumo) == true) {
                                mensagem("A ADIÇÃO FOI FEITA COM SUCESSO!\n\n");
                            } else {
                                mensagem("\nERRO!\n");
                            }
                        break;

                        case "MONOGRAFIA":
                            //Submissao subb = new Submissao;
//                            monografiaInterface.incluirMenu();
                            mono.incluirMenu();
                            break;
                    }
                    break;

                case 2:
                    switch (secao) {
                        case "ARTIGO":
                            interacao.pesquisarArtigo();
                        break;

                        case "MINICURSO":
                            interacao.pesquisarMinicurso();
                        break;

                        case "PALESTRA":
                            interacao.pesquisarPalestra();
                        break;
                            
                        case "RESUMO":
                            interacao.pesquisarResumo();
                        break;

                        case "MONOGRAFIA":
                            mono.pesquisar();
                        break;
                    }
                break;

                case 3:
                    switch (secao) {
                        case "ARTIGO":
                            Artigo art = interacao.editarArtigo();
                            if (art != null && newArtigos.editar(art.getTitulo(), art) == true) {
                                mensagem("A EDIÇÃO FOI FEITA COM SUCESSO!\n\n");
                            }
                        break;

                        case "MINICURSO":
                            Minicurso m = interacao.editarMinicurso();
                            if (m !=null && newMinicursos.editar(m.getTitulo(), m) == true){
                                mensagem("A EDIÇÃO FOI REALIZADA COM SUCESSO!\n\n");
                            } 
                        break;

                        case "PALESTRA":
                            Palestra p = interacao.editarPalestra();
                            if (p !=null && newPalestras.editar(p.getTitulo(), p) == true) {
                                mensagem("A EDIÇÃO FOI FEITA COM SUCESSO!\n\n");
                            }
                        break;

                        case "RESUMO":
                            Resumo resu = interacao.editarResumo();
                            if (resu !=null && newResumos.editar(resu.getTitulo(), resu) == true) {
                                mensagem("A EDIÇÃO FOI FEITA COM SUCESSO!\n\n");
                            }
                        break;

                        case "MONOGRAFIA":
                            String pesquisaM = lerString("um titulo para editar:");
                            Submissao subM = mono.consultarTitulo(pesquisaM);
                            mono.editar(pesquisaM, subM);
                            break;
                    }
                break;

                case 4:
                    switch (secao) {

                        case "ARTIGO":
                            interacao.excluirArtigo();
                        break;

                        case "MINICURSO":
                            interacao.excluirMinicurso();
                        break;
                            
                        case "PALESTRA":
                            interacao.excluirPalestra();
                        break;

                        case "RESUMO":
                            interacao.excluirResumo();
                        break;

                        case "MONOGRAFIA":
                            mono.excluirInterface();
                            break;
                    }
                    break;

            }
        } while (opcao != 0);
    }
}
