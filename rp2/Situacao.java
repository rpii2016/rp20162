package rp2;

/**
 *Classe enum com constantes do tipo Situaçao
 * @author Bruno, Gustavo, Mauricio, Eduardo , Julielen e Willian
 */
public enum Situacao {

     SOBAVALIACAO(1), APROVADO(2), REPROVADO(3);   
     private final int opcao;
     /**
      * Construtor da classe enum Situação que recebe um int referente a situaçao
      * @param valor 
      */
     Situacao(int valor) {
         opcao = valor;
     }
     /**
      * Metodo que retorna uma String situação formatada
      * @return Situação do Trabalho
      */
     public String getDescricao() {
            switch(this) {
                 case SOBAVALIACAO: return "Sob avaliação"; 
                 case APROVADO: return "Aprovado";
                 case REPROVADO: return "Reprovado";
                 default: return "";
            }
     }
} 