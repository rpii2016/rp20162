package palestra;

import java.util.*;
import rp2.Situacao;
import rp2.SubmissaoApresentacao;

/**
 * Classe para objetos do tipo Palestra, onde serão contidos valores e métodos para o mesmo.
 * @author Julielen
 *
 */

public class Palestra extends SubmissaoApresentacao {
    private String curriculo;
    
    
    /**
     * Construtor Classe Palestra que estende atributos da classe SubmissaoApresentacao
     * @param titulo
     * @param situacao
     * @param autores
     * @param resumo
     * @param abstractt
     * @param duracao
     * @param curriculo
     */
   
    public Palestra (String titulo, Situacao situacao, List<String> autores, String resumo, String abstractt, int duracao, String curriculo){
        
        super(titulo, situacao, autores, 1, resumo, abstractt, duracao);
        this.curriculo = curriculo;
    }
   
     /**
     * @return the curriculo
     */
    public String getCurriculo() {
        return curriculo;
    }
    /**
     * @param curriculo the curriculo to set
     */
    public void setCurriculo(String curriculo) {
        this.curriculo = curriculo;
    }

                @Override
     public String toString(){
        String resp = super.toString() + "\nCurriculo: "  + getCurriculo() +
        "\n##############################\n";
        return resp;
    }  
}