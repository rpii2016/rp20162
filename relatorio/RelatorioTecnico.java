/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relatorio;

import java.util.List;
import rp2.*;

/**
 *
 * @author Willian Lucena lucena.willian@gmail.com
 * Classe Descrevendo um Objeto do tipo Relatório Técnico.
 */
public class RelatorioTecnico extends SubmissaoCientifica {
    
    
    private String rtResumo;
    private String rtAbstract;
    private int rtAno;
    private int rtNumeroPaginas;
    
            /**
             * Método Construtor do objeto Relatório Técnico que recebe como parâmetros as seguintes informações para criar um novo Relatório:
             * @param titulo - Titulo do relatorio.
             * @param situacao - situação do relatório.
             * @param ano - ano de publicação.
             * @param autores - autores do relatório(até 4).
             * @param instituicao - instituição do relatório.
             * @param nPaginas  - número de páginas do relatório.
             * @param palavrasChave - palavras-Chave do relatório (até 4).
             * @param rAbstract - Abstract do relatório.
             * @param resumo - resumo do relatório.
       
             */
    public RelatorioTecnico(String titulo, Situacao situacao, List<String> autores, List<String> instituicao
    , List<String> palavrasChave, String resumo, String rAbstract, int ano, int nPaginas ) {
        
        super(titulo, situacao, autores, 4, instituicao, palavrasChave,1, 4);// numero 4 representa a quantidade máxima de autores.
        
        this.rtResumo = resumo;
        this.rtAno = ano;
        this.rtNumeroPaginas = nPaginas;
        this.rtAbstract = rAbstract;
        
        
    }

/**Método para retornar o Resumo do Relatório.
 * @return Resumo do relatório
 */
    public String getRtResumo() {
        return rtResumo;
    }

    /**Método para modificar o Resumo do Relatório.
     * @param rtResumo - Resumo á ser Salvo.
     */
    public void setRtResumo(String rtResumo) {
        this.rtResumo = rtResumo;
    }

    /**Método para retornar o Abstract do Relatório.
     * @return Abstract do Relatório
 */
    public String getRtAbstract() {
        return rtAbstract;
    }

    /**Método para modificar o Abstract do Relatório.
     * @param rtAbstract - Abstract a ser Salvo
     */
    public void setRtAbstract(String rtAbstract) {
        this.rtAbstract = rtAbstract;
    }

    /**Método para retornar o Ano do Relatório.
     * @return Ano de Publicação
 */
    public int getRtAno() {
        return rtAno;
    }

        /**Método para modificar o Ano do Relatório.
     * @param rtAno  - Ano a ser Salvo
     */
    public void setRtAno(int rtAno) {
        this.rtAno = rtAno;
    }

    /**Método para retornar o Número de páginas do Relatório.
     * @return Número de Páginas
 */
    public int getRtNumeroPaginas() {
        return rtNumeroPaginas;
    }

        /**Método para modificar o Número de Páginas do Relatório.
     * @param rtNumeroPaginas  - Número de páginas a ser Salvo
     */
    public void setRtNumeroPaginas(int rtNumeroPaginas) {
        this.rtNumeroPaginas = rtNumeroPaginas;
    }
    
    
    
}
