/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relatorio;

import java.util.ArrayList;
import java.util.List;
import rp2.ListaSubmissoes;
import rp2.Submissao;

/**
 *Classe que implementa a ListaSubmissoes e reescreve seus métodos especializados.
 * @author Willian Lucena lucena.willian@gmail.com
 */
public class ListaRelatoriosTecnicos implements ListaSubmissoes {

    /**
     * Lista com os Relatórios Técnicos a serem manipulados.
     */
    protected List<Submissao> listaDeRelatorios = new ArrayList<>();
    
     /**
     * Inclui um relatório a lista de relatórios.
     * @param relatorio RelatorioTecnico a ser incluído.
     * @return True se a inclusão foi bem sucedida ou False em caso contrário. 
     */
    @Override
    public boolean incluir(Submissao relatorio) {
    if (relatorio!=null) {
     listaDeRelatorios.add(relatorio);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Consulta um relatório com o título informado.
     * @param titulo Título a ser consultado.
     * @return RelatórioTécnico com o título informado ou null em caso de não existir submissão com o título informado. 
     */
    @Override
    public Submissao consultarTitulo(String titulo) {
 Submissao r = null;
        for (Submissao relatorio: listaDeRelatorios) {
            if (relatorio.getTitulo().equalsIgnoreCase(titulo)) {
                r = relatorio;
            }
        }
        return r;
    }

    /**
     * Consulta a lista de relatórios com o autor informado.
     * @param autor Autor a ser consultado.
     * @return Lista de relatórios do autor informado ou null em caso de não existir submissões do autor. 
     */
    @Override
    public List<Submissao> consultarAutor(String autor) {
    List<Submissao> relatorios = new ArrayList();
        for (Submissao relatorio: listaDeRelatorios) {
            for (int i=0; i<relatorio.getAutor().size(); i++) {
                if (relatorio.getAutor().get(i).equalsIgnoreCase(autor)) {
                    relatorios.add(relatorio);
                }
            }
        }
        return relatorios;
    }

    /**
     * Edita o relatório com o título informado.
     * @param titulo Título do relatório a ser editado.
     * @param relatorio Submissão com as novas informações.
     * @return True se o relatório com o título informado for editado e 
     * False caso nenhuma submissão com o título infornado for encontrada.
     */
    @Override
    public boolean editar(String titulo, Submissao relatorio) {
         if (consultarTitulo(titulo)!=null) {
             int index = listaDeRelatorios.indexOf(consultarTitulo(titulo));
            listaDeRelatorios.set(index, relatorio);
            return true;
        } else {
            return false;
        }
        
    }

    /**
     * Exclui o Relatorio com o título informado.
     * @param titulo Título do relatório a ser excluído.
     * @return True se o Relatório com o título informado for excluído e 
     * False caso nenhuma submissão com o título informado for encontrada.
     */
    @Override
    public boolean excluir(String titulo) {
        Submissao relatorio = consultarTitulo(titulo);
        if (relatorio != null) {
            listaDeRelatorios.remove(relatorio);
           return true;
        } else {
             return false;
        }
        
    }
    
}
