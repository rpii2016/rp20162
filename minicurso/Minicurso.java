/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minicurso;

import java.util.List;
import rp2.Situacao;
import rp2.SubmissaoApresentacao;

/**
 *
 * @author Gustavo Carvalho
 */
public class Minicurso extends SubmissaoApresentacao {
    private String recursos;
    private String metodologia;
    
    public Minicurso(String titulo, Situacao situacao, List<String> autores, int maxAutores, String resumo, String abstractt, int duracao, String recursos, String metodologia) {
        super(titulo, situacao, autores, maxAutores, resumo, abstractt, duracao);
        this.recursos = recursos;
        this.metodologia = metodologia;
    }
    
    /**
     * @return the recursos
     */
    public String getRecursos() {
        return recursos;
    }

    /**
     * @param recursos the recursos to set
     */
    public void setRecursos(String recursos) {
        this.recursos = recursos;
    }

    /**
     * @return the metodologia
     */
    public String getMetodologia() {
        return metodologia;
    }

    /**
     * @param metodologia the metodologia to set
     */
    public void setMetodologia(String metodologia) {
        this.metodologia = metodologia;
    }
    
    @Override
    public String toString(){
        String resp = super.toString() + "\nMetodologia:"  + getMetodologia()  + "\nRecursos:"  + getRecursos();
        return resp;
    }
    
}
