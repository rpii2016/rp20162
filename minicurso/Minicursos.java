package minicurso;

import java.util.ArrayList;
import java.util.List;
import static rp2.Base.*;
import rp2.ListaSubmissoes;
import rp2.Situacao;
import rp2.Submissao;
import rp2.SubmissaoApresentacao;

/**
 *
 * @author Gustavo Carvalho
 */
public class Minicursos implements ListaSubmissoes {

    private static List<Submissao> ListaMinicurso = new ArrayList<>();

    
    @Override
    public boolean incluir(Submissao submissao) {
        if (submissao==null) {
            return false;
        } else {
            ListaMinicurso.add(submissao);
            return true;
        }
    }

    @Override
    public Submissao consultarTitulo(String titulo) {
        Submissao sub = null;
        for (Submissao categoria : ListaMinicurso) {
            if (categoria.getTitulo().equalsIgnoreCase(titulo)) {
                sub = categoria;
            }
        }
        return sub;
        
    }

    @Override
    public List<Submissao> consultarAutor(String autor) {
        List<Submissao> sub = new ArrayList();
        for (Submissao categoria : ListaMinicurso) {
            for (int i = 0; i < categoria.getAutor().size(); i++) {
                if (categoria.getAutor().get(i).equalsIgnoreCase(autor)) {
                    sub.add(categoria);
                }
            }
        }
        return sub;
    }

    @Override
    public boolean editar(String titulo, Submissao submissao) {
        if (consultarTitulo(titulo)==null) {
            return false;
        } else {
            int indice = ListaMinicurso.indexOf(consultarTitulo(titulo));
            ListaMinicurso.set(indice, submissao);
            return true;
        }
    }
    
    @Override
    public boolean excluir(String titulo) {
        Minicursos minic = new Minicursos();
        Submissao sub = minic.consultarTitulo(titulo);
        if (sub==null) {
            return false;
        } else {
            ListaMinicurso.remove(sub);
        }
        return true;
    }
    
}

